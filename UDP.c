#include <WinSock2.h>

#include "UDP.H"


SOCKET socket_id; // identificatore del socket

unsigned long IP_to_bin(char ip_add[])
{
 unsigned long add;
 unsigned char byte;
 char *token;

 if ((token = strtok(ip_add,".")) == NULL)
   return 0x00000000;
 byte = (unsigned char)atoi(token);
 add = (unsigned long)byte * 16777216;
 if ((token = strtok(NULL,".")) == NULL)
   return 0x00000000;
  byte = (unsigned char)atoi(token);
  add += (unsigned long)byte * 65536;
  if ((token = strtok(NULL,".")) == NULL)
    return 0x00000000;
  byte = (unsigned char)atoi(token);
  add += (unsigned long)byte * 256;
  if ((token = strtok(NULL,".")) == NULL)
    return 0x00000000;
  byte = (unsigned char)atoi(token);
  add += (unsigned long)byte * 1;
  return add;
}

int UDP_init(unsigned short port_number)
{
 WSADATA wsaData;
 struct sockaddr_in add; // struttura per indirizzo
 unsigned long arg = 1;

 // inizializzazione WinSock (versione 2.2)
 if (WSAStartup(0x0202, &wsaData) != 0)
   return -1;
 // apertura socket UDP
 if ((socket_id = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET)
   {
    WSACleanup();
    return -1;
   }
 // costruzione struttura indirizzo
 memset(&add, 0, sizeof(add)); // azzeramento struttura
 add.sin_family = AF_INET; // dominio indirizzi IP
 add.sin_addr.s_addr = 0; // indirizzo IP locale
 add.sin_port = htons(port_number); // numero di porta assegnato
 if (bind( socket_id, (struct sockaddr *)&add, sizeof(add))== SOCKET_ERROR)
   {
    closesocket(socket_id);
    WSACleanup();
    return -1;
   }
 // impostazione del socket come non bloccante
 if (ioctlsocket(socket_id, FIONBIO, &arg) == SOCKET_ERROR)
   {
    closesocket(socket_id);
    WSACleanup();
    return -1;
   }
 return 0;
}

int UDP_send( unsigned long ip_address, unsigned short port_number, unsigned char data[], int byte)
{
 struct sockaddr_in add; // struttura per indirizzo di destinazione
 int n;

 // costruzione struttura indirizzo
 memset(&add, 0, sizeof(add)); // azzeramento struttura
 add.sin_family = AF_INET; // dominio indirizzi IP
 add.sin_port = htons(port_number); // numero di porta UDP
 add.sin_addr.s_addr = htonl(ip_address); // indirizzo IP
 // trasmissione datagram
 if ((n = sendto (socket_id,
                  (void *)data,
                  byte,
                  0,
                  (struct sockaddr*)&add,
                  sizeof(add))) < 0)
   return -1;
 return n;
}

int UDP_receive( unsigned long *ip_address, unsigned short *port_number, unsigned char data[], int size)
{
 struct sockaddr_in add; // struttura per indirizzo mittente
 size_t dim = sizeof(add);
 int n;

 // ricezione dati (non bloccante)
 if ((n = recvfrom (socket_id, (void *)data, size, 0, (struct sockaddr*)&add, &dim)) <= 0)
   return -1;
 // estrazione indirizzo IP e numero di porta UDP
 *ip_address = (unsigned long)(ntohl(add.sin_addr.s_addr));
 *port_number = (unsigned short)(ntohs(add.sin_port));
 return n;
}

void UDP_close()
{
 // chiusura socket
 closesocket(socket_id);
 // terminazione WinSock
 WSACleanup();
 return;
}
